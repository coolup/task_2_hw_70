import React from 'react';
import AddMovieForm from './containers/AddMovieForm';
import Movie from './components/Movie/Movie';
import './App.css';
import CurrentPrice from './components/CurrentPrice/CurrentPrice';

class App extends React.Component {
  state = {
    totalsum: 480,
    currentMovie: '',
    currentMovieCost: 0,
    movies: [
      { desc: 'Bottle of water', id: 1, cost: 25 },
      { desc: 'Milk', id: 2, cost: 40 },
      { desc: 'Bread', id: 3, cost: 15},
      { desc: 'Dinner at cafe', id: 4, cost: 400 }
    ],
  };
  
  AddMovieDesc = (event) => {
    const currentMovie = event.target.value;
    this.setState({ currentMovie });

  }
  AddMovieCost = (event) => {
    const currentMovieCost = event.target.value;
    this.setState({ currentMovieCost });

  }
  AddMovie = (id) => {
    const movies = [...this.state.movies];
    const index = this.state.movies.findIndex(x=>x.id === id);
    const movie = {...this.state.movies[index]};
    
    movies.unshift({
      desc: this.state.currentMovie,
      cost: this.state.currentMovieCost,      
      id: movies.length === 0 ? 0 : movies[movies.length-1].id++
    });
        
    var totalsum = this.state.totalsum;        
    totalsum = totalsum + movie.cost;
    //localStorage.setItem("movies", JSON.stringify(movies) + ',');
    this.setState({totalsum, movies});
  }
  
  removeMovie = (id) => {
    const index = this.state.movies.findIndex(m => m.id === id);
    const movies = [...this.state.movies];
    const movie = {...this.state.movies[index]}
    movies.splice(index, 1);
    var totalsum = this.state.totalsum;        
    totalsum = totalsum - movie.cost;
    this.setState({totalsum, movies });
  }

  render() {
    return (
      <div>
        <AddMovieForm newform={event => this.AddMovieDesc(event)}
                      newcost={event => this.AddMovieCost(event)} 
                      add={() => this.AddMovie(this.state.movies.length)} />
                   
        
        {
          this.state.movies.map((movie) => {
            return <Movie 
              id={movie.id}
              desc={movie.desc}
              cost={movie.cost} 
              key={movie.id} 
              remove={() => this.removeMovie(movie.id)}
              //change={event => this.changeMovie(event, movie.id)}
              /*changecost={event => this.changeCost(event, movie.id)}*/>
              </Movie>
          })}
          
          <CurrentPrice value={this.state.totalsum}/>
      </div>
    );
  }
}
export default App;