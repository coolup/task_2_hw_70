import React from 'react';
import '../components/Movie/Movie.css';

const AddMovieFrom = (props) => {
    return (
        <div className="movieform">
            <input type="text" placeholder="Item name" onChange={props.newform}/>
            <input type="number" placeholder="Cost"  onChange={props.newcost}/>          
            <input type ="button" value="Add" onClick={props.add}/>            
        </div>
    );
}
export default AddMovieFrom;