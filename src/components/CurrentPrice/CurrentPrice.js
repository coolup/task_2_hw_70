import React from 'react';

class CurrentPrice extends React.Component {
    render() {
        return <div>
            <p>Total spent: {this.props.value} KGS</p>
        </div>
    }
}

export default CurrentPrice