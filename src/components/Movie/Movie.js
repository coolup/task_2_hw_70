import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBackspace} from '@fortawesome/free-solid-svg-icons';
import './Movie.css';

class Movie extends Component {
       
    componentWillMount(){
        console.log('[Movie] Will Mount');
    }
    componentDidMount(){
        console.log('[Movie] Did Mount');
    }
    componentWillUpdate() {
        console.log('[Movie] Will Update');
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log('[Movie] ShouldUpdate')
        return nextProps.desc !== this.props.desc;
        //nextProps.cost !== this.props.cost;
    }

    render(){
        return (
            <div className="movie">
                <p>{this.props.desc} </p>
                <p>{this.props.cost} </p>
                <FontAwesomeIcon icon={faBackspace} className="icon" onClick={this.props.remove}/>
            </div>
        )
    }
}
export default Movie;